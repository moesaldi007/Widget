package smktelkommalang.widgets;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WidgetBasic2 extends AppCompatActivity {
    EditText EtNama;
    EditText EtTahun;
    Button bOk;
    TextView tvHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_basic2);

        EtNama = findViewById(R.id.editTextnama);
        EtTahun = findViewById(R.id.editTexttahun);
        bOk = findViewById(R.id.buttonok);
        tvHasil = findViewById(R.id.textViewHasil);

        bOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doprocess();

            }
        });
    }

    private void doprocess() {
        if (isValid()) {
            String nama = EtNama.getText().toString();
            int tahun = Integer.parseInt(EtTahun.getText().toString());
            int usia = 2018 - tahun;
            tvHasil.setText(nama + " berusia " + usia + " tahun");
        }
    }

    private boolean isValid() {
        boolean valid = true;
        String nama = EtNama.getText().toString();
        String tahun = EtTahun.getText().toString();

        if (nama.isEmpty()) {
            EtNama.setError("Nama belum diisi");
            valid = false;
        } else if (nama.length() < 3) {
            EtNama.setError("Nama minimal 3 karakter");
            valid = false;
        } else {
            EtNama.setError(null);

        }
        if (tahun.isEmpty()) {
            EtTahun.setError("Tahun belum diisi");
            valid = false;
        } else if (tahun.length() != 4) {
            EtTahun.setError("Format tahun bukan yyyy");
            valid = false;
        } else {
            EtTahun.setError(null);

        }
        return valid;
    }
}
